cnf ?= .env
include $(cnf)
export $(shell sed 's/=.*//' $(cnf))

.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

build: ## Build the release and develoment container. The development
	@echo "Building ..."
	@docker-compose build --force-rm --no-cache db
	@docker-compose build --force-rm --no-cache web

checkout: ## Get source code
	@echo "Getting source from repo ..."
	@rm -f html
	@mkdir html
	@echo "<?php phpinfo();" > html/index.php


up: ## Spin up the project
	@echo "Booting up ..."
	@docker-compose up -d db
	@docker-compose up -d proxy
	@docker-compose up -d letsencrypt
	@docker-compose up -d web

down: ## Tear down the project
	@echo "Tearing down ..."
	docker-compose down

login: ## Log into the web container
	@echo "Booting up ..."
	@docker exec -it web bash

logs: ## Tail and follow the logs
	@echo "Start following the logs - press CRTL-C to stop"
	@docker-compose logs -f

ps: ## Show all running docker containers
	@docker-compose ps
