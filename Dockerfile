FROM php:8-apache

RUN echo "Europe/Berlin" > /etc/timezone && dpkg-reconfigure -f noninteractive tzdata

RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -

RUN apt-get update -y && apt-get install -y libpng-dev curl libedit-dev librecode-dev libcurl4-openssl-dev libmcrypt-dev libxml2-dev libc-client-dev libpq-dev libkrb5-dev libyaml-dev libcurl3-gnutls libuuid1 libevent-dev libgearman-dev libfreetype6-dev libjpeg62-turbo-dev libpcre3-dev vim less inetutils-ping lsof mailutils mailutils-common libfreetype6-dev libjpeg62-turbo-dev libpng-dev libxslt1-dev libzip-dev unzip nodejs postgresql-client git wget

RUN docker-php-ext-install pdo_mysql gd soap pgsql pdo_pgsql mysqli zip xsl exif
RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl && docker-php-ext-install imap
RUN apt-get install -y libxml2-dev && CFLAGS="-I/usr/src/php" docker-php-ext-install xmlreader
RUN pecl install oauth && pecl install yaml && docker-php-ext-enable oauth yaml

RUN php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer
RUN /usr/bin/composer selfupdate

COPY config/000-default.conf /etc/apache2/sites-available
RUN a2dissite 000-default.conf && a2ensite 000-default.conf
RUN a2enmod rewrite
RUN service apache2 restart

RUN rm -r /var/lib/apt/lists/* && rm -f /var/log/apache2/access.log && rm -f /var/log/apache2/error.log && rm -f /var/log/apache2/other_vhosts_access.log
